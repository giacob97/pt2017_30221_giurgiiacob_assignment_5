package model;
import java.io.IOException;
import java.nio.file.*;
import java.util.stream.*;
import java.util.*;
import org.joda.time.*;
import org.joda.time.format.*;


public class MonitoredData {

	private String name;
	private DateTime start;
	private DateTime end;
	private DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

	
	
	


	public int returnDay(){
		return start.getDayOfMonth();
	}



	public MonitoredData() {
		super();
	}



	public MonitoredData(String start, String end, String name) {
		super();
		this.name = name;
		this.start = df.parseDateTime(start);
		this.end = df.parseDateTime(end);
	}


   
	public int getMinutes(){
	   return Minutes.minutesBetween(getStart(), getEnd()).getMinutes();
	}
	
	
    public List<MonitoredData> getActivities(){
       String path = "C:\\Users\\User\\Desktop\\Activities.txt";
 	   List<String> list = new ArrayList<>();
 	   List<MonitoredData> md = new ArrayList<>();
 	   try (Stream<String> stream = Files.lines(Paths.get(path))) {
 			list = stream.collect(Collectors.toList());
 		} catch (Exception exp) {
 			System.out.println(exp.getMessage());
 		}
 	   for(String str : list){
 		   String start = str.substring(0, 19);
 		   String end = str.substring(21,40);
 		   String name = str.substring(42,str.length()-1);
 		   md.add(new MonitoredData(start,end,name));
 	   }
 	   return md;
	}
    public void writeActivities(Map <String, Integer> map,String period){
		Path path = Paths.get("C:\\Users\\User\\Desktop\\TP\\HW5\\TotalNumberOfActivities.txt");
		List<String> list = new ArrayList<>();
		for (Map.Entry<String, Integer> entry : map.entrySet()) {
			list.add(entry.getKey() + "= " + entry.getValue());
		}
		list.add(period);
		try {
			Files.write(path, list);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
    public void writeActivitiesbyDays(Map<Integer ,Map <String, Integer>> map){
		Path path = Paths.get("C:\\Users\\User\\Desktop\\TP\\HW5\\ActivitiesByDays.txt");
		List<String> list = new ArrayList<>();
		for (Map.Entry<Integer,Map<String, Integer>> entry : map.entrySet()) {
			list.add("Day " + entry.getKey() +  " = " + entry.getValue().toString());
		}
		try {
			Files.write(path, list);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
	}
 
  
    public void writeActivitiesMoreThan10Hours(List<String> str,List<String> str1){
  		Path path = Paths.get("C:\\Users\\User\\Desktop\\TP\\HW5\\ActivitiesMoreThan10Hours.txt");
  		Path path1 = Paths.get("C:\\Users\\User\\Desktop\\TP\\HW5\\ActivitiesMoreThan90%Under5Minutes.txt");
  		try {
  			Files.write(path, str);
  			Files.write(path1, str1);
  		} catch (IOException e1) {
  			e1.printStackTrace();
  		}
  	}
  	
    

	public DateTime getStart() {
		return start;
	}



	public void setStart(DateTime start) {
		this.start = start;
	}



	public DateTime getEnd() {
		return end;
	}



	public void setEnd(DateTime end) {
		this.end = end;
	}



	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder("");
		str.append(getName());
		str.append(getStart().getDayOfMonth());
		str.append(getStart().getHourOfDay());
		str.append(getStart().getMinuteOfHour());
		return str.toString();
	}

}
