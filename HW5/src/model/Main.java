package model;

import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

public class Main {
	
	public static void main(String []args){
		MonitoredData m  = new MonitoredData();
		List<MonitoredData> md = new ArrayList<>();
		md = m.getActivities();
		
		Map <String, Integer> map = md.stream()
		.collect(Collectors.groupingBy(MonitoredData::getName, Collectors.summingInt(x->1)));
		
		
		Map<Integer, Map <String, Integer>> map2 = md.stream()
		.collect(Collectors.groupingBy(MonitoredData::returnDay,Collectors.groupingBy(MonitoredData::getName, Collectors.summingInt(x->1))));
	    
		m.writeActivitiesbyDays(map2);
	    m.writeActivities(map,"Distinct Days : " + String.valueOf(map2.size()));
		
	    Map<String, IntSummaryStatistics> map3 = md.stream()
		 .collect(Collectors.groupingBy(MonitoredData::getName,Collectors.summarizingInt(MonitoredData::getMinutes)));
		 
		 List<String> str = map3.entrySet().stream()
		.filter(x->x.getValue().getSum() > 600)//10h = 600 Mins 
		.map(Map.Entry::getKey)
		.collect(Collectors.toList());
		
		
		 Map<String, Integer> m3 = md.stream()
		 .filter(x -> x.getMinutes() < 5) // lasam ce e mai mic de 5 minute in fiecare
		 .collect(Collectors.groupingBy(MonitoredData::getName, Collectors.summingInt(x->1)));
	
		 
		 /*
		  * concatenam map care contine pentru fiecare cheie numarul de taskuri din toata perioada
		  * cu m3 care contine pentru fiecare activitatate toate taskurile sub 5 minute
		  * rezultatul e un map result care contine ca si cheie numele activitatii
		  * si ca si valoare un map<long,long> avand cheie numarul de act sub 5 min si ca valoare toate act.
		  * 
		  */
		 
		 Map<String,Map<Integer,Integer>> result = m3.entrySet().stream() 
				 .collect(Collectors.groupingBy(e->e.getKey(),toMap(e-> e.getValue(), e -> map.get(e.getKey()))));
		
		 List<String> list = result.entrySet().stream()
		 .filter(e-> getPerc(e.getValue())>90)		//procent mai mare de 90% 
		 .map(Map.Entry::getKey)
		 .collect(Collectors.toList());
		 m.writeActivitiesMoreThan10Hours(str,list);
	}
	
	
	
	public static int getPerc(Map<Integer,Integer> map){
		return map.entrySet().iterator().next().getKey().intValue() * 100 / map.entrySet().iterator().next().getValue().intValue();
	}
	
	}
